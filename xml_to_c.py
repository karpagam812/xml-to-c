import xml.etree.ElementTree as ET

c_file_name = 'output.c'
xml_file_name = 'sample_xml.xml'


tree = ET.parse(xml_file_name)
root = tree.getroot()

#list
data_begin = 'const DspPreset ' + xml_file_name.split('.')[0] + " = {"
clist = []
clist.append(data_begin)
compressor_list = ['  .compressor = {']
limiter_list = ['   .limiter = {']
eq1_list = ['   .eqBands[SSAP_LowPass_Index] = {']
eq2_list = ['   .eqBands[SSAP_HighPass_Index] = {']
eq3_list = ['   .eqBands[SSAP_LowShelf_Index] = {']
eq4_list = ['   .eqBands[SSAP_Bell_1_Index] = {']
eq5_list = ['   .eqBands[SSAP_Bell_2_Index] = {']

# dict
stand = {'threshold': '.threshold',
            'input-gain': '.iGain',
            'output-gain': '.oGain',
            'feedforward-feedback-toggle': '.feedFrwd',
            'attack': '.attack',
            'release': '.release',
            'ratio': '.ratio',
            'toggle': '.toggle',
            'lookahead-time': '.lookAhead'}
boost = {'frequency': '.boostCutFreq',
            'q': '.boostCutQ',
            'gain': '.boostCutGain',
            'toggle': '.boostCutEnable'}
high = {'toggle': '.hpEnable',
           'frequency': '.hpFreq'}
eq = {'type': '.type',
      'toggle': '.enabled',
     'freq': '.freq',
     'q':'.q',
     'gain':'.gain',
     'slope': '.slope'}



def convert(root, com_list, com_stand, value):
    for child in root:
        if(child.tag == 'ratio'):
            val = float(child.text.split(':')[0])/float(child.text.split(':')[1])
        elif(child.tag == 'type'):
            val = value
        else:
            val = float(child.text)
        
        name = com_stand.get(child.tag)
        text = "      " + name + " = " + str(val) + ','
        com_list.append(text)

for child in root:
    for child1 in child:
        if(child.tag == 'compressor'):
            if(child1.tag == 'standard'):
                convert(child1, compressor_list, stand, 0)
            if(child1.tag == 'boost-cut'):
                convert(child1, compressor_list, boost, 0)
            if(child1.tag == 'high-pass'):
                convert(child1, compressor_list, high, 0)
                
        elif(child.tag == 'limiter'):
            if(child1.tag == 'standard'):
                convert(child1, limiter_list, stand, 0)
            if(child1.tag == 'boost-cut'):
                convert(child1, limiter_list, boost, 0)
            if(child1.tag == 'high-pass'):
                convert(child1, limiter_list, high, 0)
    
        if(child1.tag == 'eq1'):
            value = 'SSAP_LowPass'
            convert(child1, eq1_list, eq, value)
        elif(child1.tag == 'eq2'):
            value = 'SSAP_HighPass'
            convert(child1, eq2_list, eq, value)
        elif(child1.tag == 'eq3'):
            value = 'SSAP_LowShelf'
            convert(child1, eq3_list, eq, value)
        elif(child1.tag == 'eq4'):
            value = 'SSAP_Passthrough'
            convert(child1, eq4_list, eq, value)
        elif(child1.tag == 'eq5'):
            value = 'SSAP_Bell'
            convert(child1, eq5_list, eq, value)
        
                
def merge_list(main_list, sub_list):
    for i in sub_list:
        main_list.append(i)
            
compressor_list.append("   },")
limiter_list.append("   },")
eq1_list.append("   },")
eq2_list.append("   },")
eq3_list.append("   },")
eq4_list.append("   },")
eq5_list.append("   },")

merge_list(clist, compressor_list)
merge_list(clist, limiter_list)
merge_list(clist, eq1_list)
merge_list(clist, eq2_list)
merge_list(clist, eq3_list)
merge_list(clist, eq4_list)
merge_list(clist, eq5_list)

clist.append("   .eqBands_count = EQ_BANDS_NUM,")
clist.append("};")

#Write to file
outF = open(c_file_name, "w")
for line in clist:
  outF.write(line)
  outF.write("\n")
outF.close()
